//
//  AlbardApp.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

@main
struct AlbardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
