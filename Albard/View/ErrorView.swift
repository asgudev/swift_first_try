//
//  ErrorView.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct ErrorView: View {
    var body: some View {
        Text("Error")
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView()
    }
}
