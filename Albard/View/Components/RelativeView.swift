//
//  RelativeView.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct RelativeView: View {
    let person: Person
    
    var body: some View {
        NavigationLink(
            destination: ProfileScreen(profileID: person.id),
            label: {
                HStack {
                    Image(systemName: "person")
                        .data(url: person.photo)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 100, height: 100, alignment: .leading )
                    VStack(alignment: .leading) {
                        Text(person.lastName)
                            .fontWeight(.heavy)
                            .textCase(.uppercase)
                            .foregroundColor(.black)
                        Text(person.firstName)
                            .fontWeight(.heavy)
                            .textCase(.uppercase)
                            .foregroundColor(.black)
                        Text(person.nickName)
                            .foregroundColor(.black)
                            .textCase(.uppercase)

                        Spacer()
                    }
                    Spacer()
                }
                .frame(height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .top)
                .padding(6)
            })
    }
}
