//
//  ProfileView.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct ProfileView: View {
    let person: Person
    
    var body: some View {
        
        ScrollView {
            //                GeometryReader { geo in
            ZStack(alignment: .leading) {
                Image(systemName: "person")
                    .data(url: person.photo)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                
                VStack {
                    Spacer()
                    VStack(alignment: .leading) {
                        
                        Text(person.lastName)
                            .font(.title)
                            .fontWeight(.bold)
                            .foregroundColor(Color.white)
                            .textCase(.uppercase)
                        Text(person.firstName)
                            .font(.title)
                            .foregroundColor(Color.white)
                            .textCase(.uppercase)
                        Text(person.nickName)
                            .font(.title)
                            .foregroundColor(Color.white)
                            .textCase(.uppercase)
                        
                    }.frame(minWidth: 0,
                            maxWidth: .infinity,
                            minHeight: 0,
                            maxHeight: 100,
                            alignment: .topLeading)
                    .padding(5)
                    .background(Color.black.opacity(0.5))
                }
            }
            
            .background(Color.red)
            .edgesIgnoringSafeArea(.vertical)
            
            
            Text("Parents")
                .font(.title)
            
            ForEach(person.parents!) { _person in
                RelativeView(person: _person)
            }
            
            Text("Spouses")
                .font(.title)
            
            ForEach(person.spouses!) { _person in
                RelativeView(person: _person)
            }
            
            Text("Brothers")
                .font(.title)
            
            ForEach(person.brothers!) { _person in
                RelativeView(person: _person)
            }
            
            Text("Childs")
                .font(.title)
            
            ForEach(person.childs!) { _person in
                RelativeView(person: _person)
            }
            
            
        }
        
    }
}

//struct ProfileView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileView()
//    }
//}
