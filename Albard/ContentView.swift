//
//  ContentView.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct ContentView: View {
    
    init () {
//        UINavigationBar.appearance().barTintColor = .clear
//        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    
    var body: some View {
        
        NavigationView {
            ProfileScreen(profileID: 8445) // initial ID for test
                .navigationBarTitleDisplayMode(.inline)
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        
        ContentView()
        
    }
}
