//
//  ProfileScreen.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct ProfileScreen: View {
    var profileID: Int
    
    @State var person: Person?
    
    var body: some View {
        
        VStack() {
            if (person != nil) {
                ProfileView(person: person!)
            } else {
                ProgressView()
                Text("loading")
            }
      
        }
        .onAppear {
            self.load(personID: profileID) { _person in
                self.person = _person
            }
        }
    }
    
    func load(personID: Int, completion: @escaping (Person) -> ()) {
        
        guard let url = URL(string: "https://albardaiforness.org/person/\(personID)/json") else { return }
        
        URLSession.shared.dataTask(with: url) { (data, _, _) in
            let person = try! JSONDecoder().decode(Person.self, from: data!)
            print(person)
            
            DispatchQueue.main.async {
                completion(person)
            }
        }
        .resume()
    }
}



//
//struct ProfileScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileScreen()
//    }
//}
//
