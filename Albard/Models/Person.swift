//
//  SwiftUIView.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import Foundation

struct Person: Codable, Identifiable {
    var id: Int
    var firstName: String
    var lastName: String
    var nickName: String
    var photo: URL
    var parents: [Person]?
    var spouses: [Person]?
    var brothers: [Person]?
    var childs: [Person]?



}

