//
//  PersonLoader.swift
//  Albard
//
//  Created by Philip Lukashov on 6.05.21.
//

import SwiftUI

struct PersonLoader {
    
    func getPerson(personId: Int,  completion:@escaping (Person) -> ()) -> Void {
        
        guard let url = URL(string: "https://albardaiforness.org/person/\(personId)/json") else { return }

        URLSession.shared.dataTask(with: url) { (data, _, _) in
            let person = try! JSONDecoder().decode(Person.self, from: data!)
            print(person)

            DispatchQueue.main.async {
                completion(person)
            }
        }
        .resume()
        /*
        
        if (personId == 8445) {
            complete(Person(
                id: 8445,
                firstName: "bruno tommaso luigi",
                lastName: "de santa",
                nickName: "guol - pitaciu - ciuffo",
                photo: URL(string: "https://pyxis.nymag.com/v1/imgs/f42/5ea/b4f2d286f3d485fb187eef60bc825c752f-20-natalie-dormer-2.rsquare.w700.jpg")!
            ))
        } else {
            complete(Person(
                id: 8446,
                firstName: "realatime",
                lastName: "realatime",
                nickName: "realatime",
                photo: URL(string: "https://pyxis.nymag.com/v1/imgs/f42/5ea/b4f2d286f3d485fb187eef60bc825c752f-20-natalie-dormer-2.rsquare.w700.jpg")!
            ))
        }
        
    
         

         guard let url = URL(string: "https://albardaiforness.org/person/\(id)/json") else { return }

         URLSession.shared.dataTask(with: url) { (data, _, _) in
             let person = try! JSONDecoder().decode(Person.self, from: data!)
             print(person)

             DispatchQueue.main.async {
                 completion(person)
             }
         }
         .resume()
        

         
         
         */
        
        
    }
}
