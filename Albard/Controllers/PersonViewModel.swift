import Foundation

class PersonViewModel: ObservableObject {
//    enum State {
//        case idle
//        case loading
//        case failed(Error)
//        case loaded(Person)
//    }
    
//    @Published private(set) var state = State.idle
    
//    @Published var person: Person = Person()
    
    private let personID: Int
    @Published private var person: Person!
//    private let loader: PersonLoader
    
    init(personID: Int) {
        self.personID = personID
//        self.loader = loader
    }
    
    func load(personID: Int, completion: @escaping (Person) -> ()) {
//        state = .loading
//
//        loader.getPerson(personId: personID) { result in
//            print("\(result)")
//        }
        
        guard let url = URL(string: "https://albardaiforness.org/person/\(personID)/json") else { return }

        URLSession.shared.dataTask(with: url) { (data, _, _) in
            let person = try! JSONDecoder().decode(Person.self, from: data!)
            print(person)

            DispatchQueue.main.async {
                completion(person)
            }
        }
        .resume()
    }
}
